var createError = require('http-errors');
var express = require('express');
var path = require('path');
var lessMiddleware = require('less-middleware');
var logger = require('morgan');
var ejs =require('ejs');
var login = require('./routes/login');
var home = require('./routes/home');
var result = require('./routes/result');
var profile = require('./routes/profile');
var recipe = require('./routes/recipe');
var upload = require('./routes/upload');
var logout = require('./routes/logout');
var session = require('express-session');
var cookieParser = require('cookie-parser');

var app = express();
app.use(session({
    secret :  'test',
    resave : true,
    saveUninitialized: false,
    cookie : {
        maxAge : 1000 * 60 * 3,
    },
}));
// app.engine('html',ejs.__express);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser('test'));

app.use('/', login);
app.use('/login', login);
app.use('/home', home);
app.use('/result', result);
app.use('/profile', profile);
app.use('/recipes', recipe);
app.use('/upload', upload);
app.use('/logout', logout);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
