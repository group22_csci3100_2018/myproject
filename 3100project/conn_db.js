var mysql = require('mysql');
var express = require('express');
var app = express();
var con = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "csci3100",
});
//connect to the db
con.connect(function(err){
	if (err) throw err;
	console.log("################connected!################");
});

//create database
/*
con.query("CREATE DATABASE mydb", function (err, result) {
    if (err) throw err;
    console.log("############Database created###############");
});
*/

// use CSCI db
con.query("USE CSCI");

/* 
=============== table USER ==================
USER:
- uid (int) [pk]
- uname (str) [not null, unique]
- email (str) [not null]
- password (str) [not null]
*/
// create table USER
var sql = "create table USER (" +
    "uid int, uname varchar(30) not null unique, " +
    "email varchar(50) not null, password varchar(30) not null, " +
    " primary key (uid))";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############### created table USER ################");
    console.log(results);

});

// sample attributes values of USER
uid = 1;
uname = 'jason';
email = 'jasonli@cse.com';
pw = 'pass';

// insert into USER
//sql = "insert into USER values(1,'jason','123@cse.com','pass')";
con.query('insert into USER values (${uid}, ${uname}, ${email}, ${pw})', 
    function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("########### inserted 1 record into USER ############");
    console.log(results);
});

// query in USER
sql = "select * from USER";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############### querying USER ################");
    console.log(results);
});

// drop table USER
sql = "drop table USER";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############### dropped table USER ################");
    console.log(results);
});



/* 
=============== table RECIPE ==================
RECIPE:
- rid (int) [pk]
- uid (int) [fk]
- title (str) [not null]
- udate (date)
- hearts (int) [default = 0]
*/
// create table RECIPE
var sql = "create table RECIPE (" +
    "rid int, uid int, title varchar(50) not null, "+
    "udate date, hearts int default 0, primary key (rid), " +
    "foreign key (uid) references USER(uid))";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## created table RECIPE ###############");
    console.log(results);

});

// sample attributes values of RECIPE
rid = 1;
ruid = 1;
title = 'Wonderful Pizza';
udate = '2018-03-18'

// insert into RECIPE
con.query('insert into RECIPE (rid, uid, title, udate) values (${rid}, ${ruid}, ${title}, ${udate})',
    function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("########### inserted 1 record into RECIPE ############");
    console.log(results);
});

// query in RECIPE
sql = "select * from RECIPE";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## querying RECIPE ################");
    console.log(results);
});

// drop table RECIPE
sql = "drop table RECIPE";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## dropped table RECIPE ################");
    console.log(results);
});



/* 
============= table INGREDIENT ===============
INGREDIENT:
- rid (int) [pk, fk]
- iname (str) [pk]
- amount (str)
*/
// create table INGREDIENT
var sql = "create table INGREDIENT (" +
    "rid int, iname varchar(50), "+
    "amount varchar(50), primary key (rid, iname)," +
    "foreign key (rid) references RECIPE(rid))";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## created table INGREDIENT ###############");
    console.log(results);

});

// sample attributes values of INGREDIENT
irid = 1;
iname = 'mozzarella cheese';
amount = '100g';

// insert into INGREDIENT
con.query('insert into INGREDIENT values (${irid}, ${iname}, ${amount})',
    function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("########### inserted 1 record into INGREDIENT ############");
    console.log(results);
});

// query in INGREDIENT
sql = "select * from INGREDIENT";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## querying INGREDIENT ################");
    console.log(results);
});

// drop table INGREDIENT
sql = "drop table INGREDIENT";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## dropped table INGREDIENT ################");
    console.log(results);
});



/* 
============== table METHOD =================
METHOD:
- rid (int) [pk, fk]
- stepnum (int) [pk]
- detail (str) [not null]
*/
// create table METHOD
var sql = "create table METHOD (" +
    "rid int, stepnum int, "+
    "detail varchar(200) not null, primary key (rid, stepnum)," +
    "foreign key (rid) references RECIPE(rid))";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## created table METHOD ###############");
    console.log(results);

});

// sample attributes values of METHOD
mrid = 1;
stepnum = 1;
detail = 'In a large bowl, mix all ingredients using an electric mixer.';

// insert into METHOD
con.query('insert into METHOD values (${mrid}, ${stepnum}, ${detail})',
    function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("########### inserted 1 record into METHOD ############");
    console.log(results);
});

// query in METHOD
sql = "select * from METHOD";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## querying METHOD ################");
    console.log(results);
});

// drop table METHOD
sql = "drop table METHOD";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## dropped table METHOD ################");
    console.log(results);
});



/* 
============== table COMMENT =================
COMMENT:
- rid (int) [pk, fk]
- uid (int) [pk]
- content (str) [pk]
*/
// create table COMMENT
var sql = "create table COMMENT (" +
    "rid int, uid int, "+
    "content varchar(500) not null," + 
    "primary key (rid, uid, content)," +
    "foreign key (rid) references RECIPE(rid)," +
    "foreign key (uid) references USER(uid))";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## created table COMMENT ###############");
    console.log(results);

});

// sample attributes values of COMMENT
crid = 1;
cuid = 1;
content = 'Great recipe! Imma try it tonight!';

// insert into COMMENT
con.query('insert into COMMENT values (${crid}, ${cuid}, ${content})',
    function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("########### inserted 1 record into COMMENT ############");
    console.log(results);
});

// query in COMMENT
sql = "select * from COMMENT";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## querying COMMENT ################");
    console.log(results);
});

// drop table COMMENT
sql = "drop table COMMENT";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## dropped table COMMENT ################");
    console.log(results);
});



/* 
============== table SAVERECIPE =================
SAVERECIPE:
- uid (int) [pk, fk]
- rid (int) [pk, fk]
*/
// create table SAVERECIPE
var sql = "create table SAVERECIPE (" +
    "uid int, rid int, "+
    "primary key (rid, uid)," +
    "foreign key (uid) references USER(uid)," +
    "foreign key (rid) references RECIPE(rid))";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## created table SAVERECIPE ###############");
    console.log(results);

});

// sample attributes values of SAVERECIPE
suid = 1;
srid = 2;

// insert into SAVERECIPE
con.query('insert into SAVERECIPE values (${suid}, ${srid})',
    function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("########### inserted 1 record into SAVERECIPE ############");
    console.log(results);
});

// query in SAVERECIPE
sql = "select * from SAVERECIPE";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## querying SAVERECIPE ################");
    console.log(results);
});

// drop table SAVERECIPE
sql = "drop table SAVERECIPE";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## dropped table SAVERECIPE ################");
    console.log(results);
});



/* 
============== table TAG =================
TAG:
- rid (int) [pk, fk]
- tname (str) [pk]
*/
// create table TAG
var sql = "create table TAG (" +
    "rid int, tname varchar(30), "+
    "primary key (rid, tname)," +
    "foreign key (rid) references RECIPE(rid))";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## created table TAG ###############");
    console.log(results);

});

// sample attributes values of TAG
trid = 1;
tname = 'pizza';

// insert into TAG
con.query('insert into TAG values (${trid}, ${tname})',
    function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("########### inserted 1 record into TAG ############");
    console.log(results);
});

// query in TAG
sql = "select * from TAG";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## querying TAG ################");
    console.log(results);
});

// drop table TAG
sql = "drop table TAG";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############## dropped table TAG ################");
    console.log(results);
});



/* 
============== Query SQL statements =================
*/

// Get password if input matches uname
_uname = 'jason';
sql = "select distinct password from USER where uname=${_uname}";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("######## Get password if input matches uname ########");
    console.log(results);
});

// Give a heart to a recipe
_rid = 1;
sql = "update RECIPE set hearts=hearts+1 where rid=${_rid}";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("############# Give a heart to a recipe #############");
    console.log(results);
});

// Retrieve a user's basic info (uname, email)
_uid = 1;
sql = "select distinct uname, email from USER where uid=${_uid}";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("#### Retrieve a user's basic info (uname, email) ####");
    console.log(results);
});

// Retrieve a user's saved recipes
_uid = 1;
sql = "select distinct rid from SAVERECIPE where uid=${_uid}";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("########## Retrieve a user's saved recipes ##########");
    console.log(results);
});

// Retrieve a user's uploaded recipes
_uid = 1;
sql = "select distinct rid, title, udate, hearts from RECIPE where uid=${_uid}";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("######### Retrieve a user's uploaded recipes #########");
    console.log(results);
});

// Search recipes (title / hashtag)
_keyword = 'pizza';
sql = "select distinct r.rid, r.title from RECIPE as r " + 
    "where lower(r.title) like '%${_keyword}%' " +
    "union " +
    "select distinct r.rid, r.title from RECIPE as r, TAG as t " + 
    "where r.rid=t.rid and lower(t.tname) like '%${_keyword}%'";
con.query(sql, function(error, results) {
    if (error) {
        return console.error(error);
    }
    console.log("######### Search recipes (title / hashtag) #########");
    console.log(results);
});


// end db connection
con.end();