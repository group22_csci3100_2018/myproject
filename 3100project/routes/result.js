var express = require('express');
var router = express.Router();
/*
This part is to to establish connection to mysql database for access in the future
*/
var mysql = require('mysql');
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "csci3100",
    database: "csci"
});
con.connect(function(err){
    if (err){
        console.log('Error connecting to Db');
        throw err;
    }
    console.log("################connected DATABASE!################");
});
/* GET home page. */
// for ( var i= <%= result.length %>;i>0;i--){
//     //     var role =( <%= result[0]. %>)
//     // }
//     //
//     //     var txt = '<div class="col">';
//     //     txt+='<div class="card recipe-box">';
//     //     txt+='<a href="/recipes/'+ <%=role.rid %>+'.html">';
//     //     txt+='<div class="card-body text-center recipe-box">';
//     //     txt+='<img class="crop" src="/images/chicken.jpg" alt="">';
//     //     txt+='<h5 class="recipe_name">'+role.title+'</h5>';
//     //     txt+='</div></a></div></div>';
//     //     alert(txt);
//     //     document.getElementById("result").innerHTML+=txt;
//     // }

/*
This part is to receive search call from client at home page
*/
router.get('/', function(req, res, next) {
    // if need log in first then uncomment these
    if(req.session.user){
        var key=req.query.search;
        console.log(key);
        if(req.query){
            pattern = "%"+key+"%";
            console.log("pattern format: "+pattern);
            con.query("select distinct tag.rid, recipe.title from tag,recipe where (title like ? or tname = ? ) and tag.rid = recipe.rid ;",[pattern,key], function(err, rows){
                if(err) {
                    console.log("sthwrong");
                    throw err;
                } else {
                    //if there exist result
                    if (rows.length > 0) {
                        if (rows)
                            console.log(rows);
                        for(var i=0;i<rows.length;i++){
                            console.log("related recipe rid: "+rows[i].rid);
                            /*
                            call to search for related recipe.ejs                           
                            */
                            searchfile(rows[i].rid);   
                        }
                        req.result=rows;
                        req.index=0;
                        res.render('result',req);
                        console.log("thats all");

                    }else{
                        console.log("no such result");
                        rows.length=0;
                        req.result=rows;
                        req.index=0;
                        res.render('result',req);

                    }
                }
            });
        }else {
            req.user=req.session.user;
            res.render('result', req);
        }
    }else{
        res.redirect('/login');
    }
    //res.render('result', { title: 'RESULT' });
});
/*
search through the recipe folder to find related recipe
use datapacket to store recipe file that fulfil the search keyword
*/
function searchfile(j){
  console.log("searching " + j);  
    glob("../views/recipes/" + j + "*", function (er, files) {      
        console.log("globbing " + j);
        // files is an array of filenames.
        // If the `nonull` option is set, and nothing
        // er is an error object or null.
        if (er) {
            // omg something went wrong
            console.log("SHITTTT");
            throw new Exception(er);
        }
        // do something with the required files
        //console.log(files[0]);
        datapacket.push(files[0]);
        console.log("currently in datapackt: "+datapacket);
    });       
}

module.exports = router;