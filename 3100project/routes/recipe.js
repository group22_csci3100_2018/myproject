var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/:name', function(req, res, next) {
    // if need log in first then uncomment these
    if(req.session.user){
        req.user=req.session.user;
        var url ='recipes/'+req.params.name;
        res.render(url, req);
    }else{
        res.redirect('/login');
    }
    //res.render('recipe', { title: 'RECIPE' });
});

/*
receive /saverecipe POST signal
insert the rid into user's profile 
*/
router.get('/saverecipe', function(req, res, next) {
	console.log("GOT POST SIGNAL");
	var name=req.body.recipeName;
	con.query("select user.uid,recipe.rid from recipe,user,saverecipe where recipe.title=?,user.uid !=recipe.uid",[name], function(err, rows){
		if(err) {
			throw err;
		} else {               
				//if there exist result
				if (rows.length > 0) {								
				con.query("INSERT INTO saverecipe VALUES (?,?); ",[row[0],row[1]])
				}
            }
        });
});

/*
receive /comment POST signal from client
insert the string into comment database
*/
router.get('/comment', function(req, res, next) {
    console.log("GOT POST SIGNAL");
    var comment=req.body.userComment;
    var recipeID=req.body.recipeID;
    var userID = req.session.user;
    con.query("insert into comment values (?,?,?)",[recipeID,userID,comment], function(err, rows){
        if(err) {
            throw err;
        }
    });
});

module.exports = router;
