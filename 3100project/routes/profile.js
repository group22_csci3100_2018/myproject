var express = require('express');
var router = express.Router();
/*
This part is to to establish connection to mysql database for access in the future
*/
var mysql = require('mysql');
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "csci3100",
    database: "csci"
});
con.connect(function(err){
    if (err){
        console.log('Error connecting to Db');
        throw err;
    }
    console.log("################connected DATABASE!################");
});
/* 
dispplay number of recipe for this user identified by user session
*/
router.get('/', function(req, res, next) {
    // if need log in first then uncomment these
        if(req.session.user){
            con.query("select count(*) as number from saverecipe where uid=?",[req.session.user.uid], function(err,rows){
                if(err) {
                    throw err;
                } else {
                    con.query("select count(*) as number from recipe where uid=?",[req.session.user.uid], function(err,result){
                        if(err) {
                            throw err;
                        }else{
                            req.session.user.saverecipe=rows[0].number;
                            req.session.user.uploaded=result[0].number;
                            req.user=req.session.user;
                            res.render('profile', req);
                        }
                    });

                }
            })
        }else{
            res.redirect('/login');
        }
});
router.post('/:name', function(req, res) {
    if(req.params.name=="upload"){
        console.log("GOT uploaded recipe POST");
        con.query("select rid,title from recipe where uid=?",[req.session.user.uid], function(err,rows){
            if(err) {
                throw err;
            }else{
                // res.end(rows);
                return res.send(rows);
            }
        });
    }
    if(req.params.name=="save"){
        console.log("GOT save recipe POST");
        con.query("select saverecipe.rid,recipe.title from saverecipe,recipe where recipe.rid=saverecipe.rid and saverecipe.uid=?",[req.session.user.uid],function(err,results){
            if(err) {
                throw err;
            }else{

                return res.send(results);
            }
        });
    }


});
// var title=data[0].title;
// for ( var i= 0;i<<%= user.uploaded %>;i++){
//     var txt= '<ul class="list-group"><a href="/recipe/<%= rid %>.html"> <li class="list-group-item"><%= title %></li> </a></ul>';
//     document.getElementById("upload").innerHTML+=txt;
// }
module.exports = router;