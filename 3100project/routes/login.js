var express = require('express');
var router = express.Router();
var bodyParser = require("body-parser");
var express =require('express-session');
/*
This part is to to establish connection to mysql database for access in the future
*/
var mysql = require('mysql');
var con = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "csci3100",
    database: "csci"
});
con.connect(function(err){
    if(err){
        console.log('Error connecting to Db');
        throw err;
    }
    console.log("################connected DATABASE!################");
});

/* GET home page. */
router.get('/', function(req, res, next) {
    if(req.session.user){
        req.user=req.session.user;
        res.render('index',req);
    }else{
        res.render('signin',req);
    }
});

module.exports = router;
/*
To receive /login POST signal 
search database by given email then password
if exist user
    if password correct
        create an user session
        redirect to home page
    else
        alert wrong password
else
    alert no such user
*/
router.post('/login',function(req,res){
	console.log("GOT POST SIGNAL");
	var email=req.body.user;
	var password=req.body.password;
	console.log("email = "+email+", password is "+password);
	con.query("select * from user where email=?",[email], function(err, rows){
		if(err) {
			throw err;
		} else {
                //get user info for the 1st row                
				//if there exist result
				if (rows.length > 0) {
					if (rows)
						console.log("Test:" + rows);
					console.log(rows);
					if (rows[0].password === password) {
						console.log("correct!");
						//initialize the user info
                        req.session.user= {uid: rows[0].uid, username:rows[0].uname,email:rows[0].email, saverecipe: 0,uploaded:0};
                        res.end("done");

					} else {
                        res.end("incorrect password");
					}
				}else{
					console.log("no such user");

					res.end('no such user');
				}
            }
        });
	// res.end("done");
});

/*
To receive /signup signal and related data
insert user name and email into database if no identical name is found    
*/
router.post('/signup',function(req,res){
	console.log("GOT SIGNUP SIGNAL");
	var uname=req.body.user;
	var password=req.body.password;
	var email=req.body.email;
	console.log("email  = "+email+", password is "+password+" name is "+uname);
    con.query("select * from user where email=?",[email], function(err, rows){
        if(err) {
            throw err;
        } else {
            if (rows.length > 0) {
                if (rows)
                    console.log("Test:" + rows);
                console.log(rows);
                res.end("email already used");
            }else{
                console.log("check uname");
                con.query("select * from user where uname=?",[uname], function(err, rows){
                    if(err) {
                        throw err;
                    } else {
                        if (rows.length > 0) {
                            if (rows)
                                console.log("Test:" + rows);
                            console.log(rows);
                            res.end("username already used");
                        }else{
                            con.query("insert into user (uname,email,password) values(?,?,?)",[uname,email,password],function(err){
                                //con.query("insert into user (uid,uname,email,password) values(?,?,?,?)",1,[uname],[email],[password], function(err, res){
                                if(err) {
                                    console.log("sthingwrong");
                                    throw err;
                                }else{
                                    console.log("insert success!");
                                    res.end("signup success");
                                }

                            });
                        }
                    }
                });
            }
        }
    });
});

