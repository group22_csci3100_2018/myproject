var express = require('express');
var router = express.Router();
var session = require('express-session');
var bodyParser = require("body-parser");
//for search in file
var glob = require("glob");    
//array storing path to search result html                
var datapacket =[];	
/*
This part is to to establish connection to mysql database for access in the future
*/
var mysql = require('mysql');
var con = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "csci3100",
	database: "csci"
});
con.connect(function(err){
	if (err){
		console.log('Error connecting to Db');
		throw err;
	}
	console.log("################connected DATABASE!################");
});

/* GET home page. */
router.get('/', function(req, res) {
	console.log("hi");
    // if need log in first then uncomment these
    if(req.session.user){
        req.user=req.session.user;
        res.render('index',req);
    }else{
        res.redirect('/login');
    }
    // res.render('index',{ title: 'HOME' })

});





module.exports = router;


