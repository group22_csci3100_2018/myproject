<!--<Program Name:POP up box>-->
<!--<Programer: Poon King Hin>-->
<!--<Version 1>-->
<!--<Purpose:It is the function for the button in upload page)>-->
<!--<Continue:when the button is being clicked>-->
<!--<Continue:2 box which can take input will be create and display under the pervious one>-->
<!--<Continue:It also have a cancel button at the right most of the new added line>-->
$(document).ready(function () {
    var counter = 0;
    var counter2 = 0;
    $("#addrow").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";

        cols += '<td><input type="text" class="form-control" name="name' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control"  style="width:150px;" name="Amounts' + counter + '"/></td>';

        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        counter++;
    });


    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1
    });


    $("#addrow2").on("click", function () {
        var newRow2 = $("<tr>");
        var cols2 = "";

        cols2 += '<td><input type="text" class="form-control" name="Steps' + counter2 + '"/></td>';
        cols2 += '<td><input type="text" class="form-control method_width"  name="Method' + counter2 + '"/></td>';

        cols2 += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
        newRow2.append(cols2);
        $("table.order-list2").append(newRow2);
        counter2++;
    });

    $("table.order-list2").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter2 -= 1
    });
});




